#!/bin/bash

SCRIPT_DIR=$(cd $(dirname $0); pwd)
cd "${SCRIPT_DIR}/../example/"

FILES="$(find ./ -name "*.ipynb")"

# ------------------------------------------------------------------------------------------------------------------
#
#   Main
#
# ------------------------------------------------------------------------------------------------------------------
for file in ${FILES[@]}; do
  jupyter nbconvert --to notebook --execute "${file}" --stdout >/dev/null
  status="${?}"
  if [ ${status} -ne 0 ]; then
    exit ${status}
  fi
done
