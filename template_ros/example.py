from __future__ import annotations

import time

import rospy
import std_msgs.msg as std_msgs


def make_header(seq: int = 0, stamp: (rospy.Time | None) = None, frame_id: str = ""):
    if stamp is None:
        float_secs = time.time()
        secs = int(float_secs)
        nsecs = int((float_secs - secs) * 1000000000)
        stamp = rospy.Time(secs, nsecs)

    return std_msgs.Header(seq, stamp, frame_id)
