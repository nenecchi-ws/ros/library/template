<!----------------------------------------------------------------------------------------------------------------------
#
#   Title
#
# --------------------------------------------------------------------------------------------------------------------->
# Template: ROS Python Library 

<!----------------------------------------------------------------------------------------------------------------------
#
#   Badge
#
# --------------------------------------------------------------------------------------------------------------------->
[![pipeline status](https://gitlab.com/nenecchi-ws/ros/library/template/badges/main/pipeline.svg)](https://gitlab.com/nenecchi-ws/python/template/-/commits/main)
[![coverage report](https://gitlab.com/nenecchi-ws/ros/library/template/badges/main/coverage.svg)](https://gitlab.com/nenecchi-ws/python/template/-/commits/main)
[![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black)
[![Doc style: Google](https://img.shields.io/badge/%20style-google-3666d6.svg)](https://google.github.io/styleguide/pyguide.html)
[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT)

<!----------------------------------------------------------------------------------------------------------------------
#
#   Description
#
# --------------------------------------------------------------------------------------------------------------------->
This is a template package of ROS Python Library that contains CI tests in GitLab.

![Input Image](docs/image.png)

<!----------------------------------------------------------------------------------------------------------------------
#
#   Table of Contents
#
# --------------------------------------------------------------------------------------------------------------------->
## Table of Contents
  * [Requirement](#requirement)
  * [Getting Started](#getting-started)
    * [Installation](#installation)
  * [Usage](#usage)
  * [Example](#example)
  * [Contributing](#contributing)
  * [License](#license)
<!----------------------------------------------------------------------------------------------------------------------
#
#   Requirement
#
# --------------------------------------------------------------------------------------------------------------------->
## Requirement
* Required
  * Ubuntu 20.04
  * Python 3.8
  * ROS Noetic
* Recommends
  * etc ... (e.g., nvidia-driver >= 440, CUDA >= 11.3)

<!----------------------------------------------------------------------------------------------------------------------
#
#   Getting Started
#
# --------------------------------------------------------------------------------------------------------------------->
## Getting Started

### Installation
This project has not been released on PyPI.  

#### pip
Install from the remote repository.
```shell
pip install git+https://gitlab.com/nenecchi-ws/ros/library/template.git
```

#### Clone
Clone to your project by `git clone` or `git submodule` and make a path in some way (e.g., Symbolic link).
```shell
git clone https://gitlab.com/nenecchi-ws/ros/library/template.git
```
```shell
git submodule add https://gitlab.com/nenecchi-ws/ros/library/template.git
```

<!----------------------------------------------------------------------------------------------------------------------
#
#   Usage
#
# --------------------------------------------------------------------------------------------------------------------->
## Usage

```python
from template_ros import make_header

# <class 'std_msgs.msg._Header.Header'>
msg = make_header(frame_id="test")
```

<!----------------------------------------------------------------------------------------------------------------------
#
#   Example
#
# --------------------------------------------------------------------------------------------------------------------->
## Example
The example codes based on the Jupyter Notebook are provided.
* [example.ipynb](example/example.ipynb)

<!----------------------------------------------------------------------------------------------------------------------
#
#   Contributing
#
# --------------------------------------------------------------------------------------------------------------------->
## Contributing
See [CONTRIBUTING](CONTRIBUTING.md) to learn about CI.

<!----------------------------------------------------------------------------------------------------------------------
#
#   License
#
# --------------------------------------------------------------------------------------------------------------------->
## License
MIT License (see [LICENSE](LICENSE)).
