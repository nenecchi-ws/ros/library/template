from __future__ import annotations

import pytest
from _pytest.config import Config
from _pytest.config.argparsing import Parser
from _pytest.python import Function


def pytest_addoption(parser: Parser):
    parser.addoption("--all", action="store_true", default=False, help="run slow tests")


def pytest_configure(config: Config):
    config.addinivalue_line("markers", "slow: mark test as slow to run")


def pytest_collection_modifyitems(config: Config, items: list[Function]):
    if config.getoption("--all"):
        return

    skip_slow = pytest.mark.skip(reason="need --all option to run")
    for item in items:
        if "slow" in item.keywords:
            item.add_marker(skip_slow)
