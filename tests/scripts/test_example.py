from __future__ import annotations

import time

import pytest
import rospy

from template_ros import make_header


class TestExample:
    @pytest.mark.parametrize("seq, frame_id", [(1, "A"), (2, "B")])
    def test_args(self, seq: int, frame_id: str):
        float_secs = time.time()
        secs = int(float_secs)
        nsecs = int((float_secs - secs) * 1000000000)
        stamp = rospy.Time(secs, nsecs)

        msg = make_header(seq, stamp, frame_id)
        assert msg.seq == seq
        assert msg.frame_id == frame_id

    def test_stamp(self):
        msg1 = make_header()
        msg2 = make_header()
        assert msg1.stamp.nsecs != msg2.stamp.nsecs
